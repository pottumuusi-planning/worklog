Worklog
=======

In progress {#in_progress}
-----------

-   \[o\] New blanket

`   * [X] Measure`\
`       * 248 x 178 cm`\
`   * [ ] Buy`

Cache
-----

-   \[ \] Car repairs for inspection

`   * Inspection valid until: 4.10.2023`\
`   * [ ] Fuel leak`\
`   * [ ] Headlights`\
`       * [ ] Search for viiksi from autopurkaamot`

-   \[ \] Clean apartment

`   * [ ] Organize items`\
`   * [ ] Wipe dust`\
`   * [ ] Wipe surfaces`\
`   * [ ] Vacuum`\
`   * [ ] Mop`

-   \[ \] Food from store
-   \[ \] Tidy up
-   \[ \] Visit

Backlog
-------

-   \[ \] Console game streaming setup
-   \[ \] Get sufficient amount of electrical wire

`   * [ ] Find out required wire gauges`\
`   * [ ] Find out some required lengths`

Done
----
